Pour la pipeline Gitlab, nous avons utilisé  : 
 - lint
 - jest

Pour éviter tout conflits des dépendances, nous avons utilisé cette c ommande lors de l'installation avec npm

    npm install --legacy-peer-deps

Générer à chaque pipeline la nouvelle version de votre application dans un conteneur situé dans un Gitlab registry.

    docker build -t registry.gitlab.com/druxys/start-ui-native

